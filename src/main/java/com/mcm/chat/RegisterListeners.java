package com.mcm.chat;

import com.mcm.chat.listeners.ChatLocalEvent;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new ChatLocalEvent(), Main.plugin);
    }
}
