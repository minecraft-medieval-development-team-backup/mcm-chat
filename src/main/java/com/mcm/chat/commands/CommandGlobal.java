package com.mcm.chat.commands;

import com.mcm.chat.cache.ChatTimer;
import com.mcm.core.Main;
import com.mcm.chat.utils.ChatUtils;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.ChatStatus;
import com.mcm.core.enums.ServersList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class CommandGlobal implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        if (command.getName().equalsIgnoreCase("global") || command.getName().equalsIgnoreCase("g")) {
            if (args.length > 0) {
                if (ChatTimer.get(uuid + "global") != null) {
                    if ((new Date().getTime() - ChatTimer.get(uuid + "global").getDate().getTime()) < 15) {
                        player.sendMessage(ChatColor.RED + Main.getTradution("eYX2S6Asfx*cYaY", uuid) + (new Date().getTime() - ChatTimer.get(uuid + "tell").getDate().getTime()) + Main.getTradution("tcyRRGrkh5uV8%x", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return false;
                    } else ChatTimer.get(uuid + "global").delete();
                }

                String msg = "";
                for (final String txt : args) {
                    msg = msg + txt + " ";
                }
                msg.replaceAll("§", "&");

                if (com.mcm.core.database.ConfigDb.get(uuid).getChatGlobal() == false) {
                    player.sendMessage(Main.getTradution("%4jqEP!H62sQKcg", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return false;
                }

                String[] perm = {"superior", "gestor", "moderador", "suporte"};
                if (ChatStatus.getStatus() == false && !Arrays.asList(perm).contains(tag)) {
                    player.sendMessage(Main.getTradution("T9ueabA*Edan@Ru", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return false;
                }

                if (!Arrays.asList(perm).contains(tag)) new ChatTimer(uuid + "global").insert();
                if (ChatUtils.tagMsg(tag) != null) {
                    player.sendMessage(ChatColor.GRAY + "[G] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                } else {
                    player.sendMessage(ChatColor.GRAY + "[G] " + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                }

                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (com.mcm.core.database.ConfigDb.get(target.getUniqueId().toString()).getChatGlobal() && !target.getName().equals(player.getName())) {
                        if (ChatUtils.tagMsg(tag) != null) {
                            target.sendMessage(ChatColor.GRAY + "[G] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                        } else {
                            target.sendMessage(ChatColor.GRAY + "[G] " + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                        }
                    }
                }

                for (ServersList server : ServersList.values()) {
                    if (!com.mcm.core.Main.server_name.equals(server.name().replaceAll("_", "-")))
                        if (ChatUtils.tagMsg(tag) != null) {
                            RabbitMq.send(server.name().replaceAll("_", "-"), ChatColor.GRAY + "[G] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                        } else {
                            RabbitMq.send(server.name().replaceAll("_", "-"), ChatColor.GRAY + "[G] " + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                        }
                }
            }
        }
        return false;
    }
}
