package com.mcm.chat.commands;

import com.mcm.chat.utils.ChatUtils;
import com.mcm.core.RabbitMq;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.ServersList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Arrays;

public class CommandStaff implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor", "moderador", "suporte"};

        if (command.getName().equalsIgnoreCase("s") || command.getName().equalsIgnoreCase("staff")) {
            if (Arrays.asList(perm).contains(tag)) {
                String msg = "";
                for (final String txt : args) {
                    msg = msg + txt + " ";
                }
                msg.replaceAll("§", "&");

                player.sendMessage(ChatColor.RED + "[STAFF] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));

                for (Player target : Bukkit.getOnlinePlayers()) {
                    if (Arrays.asList(perm).contains(com.mcm.core.database.TagDb.getTag(target.getUniqueId().toString())) && !player.getName().equals(target.getName())) {
                        target.sendMessage(ChatColor.RED + "[STAFF] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                    }
                }
                for (ServersList server : ServersList.values()) {
                    if (!com.mcm.core.Main.server_name.equals(server.name().replaceAll("_", "-")))
                        RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/chat_staff=" + ChatColor.RED + "[STAFF] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.GRAY + ChatUtils.permColor(msg, tag));
                }
            }
        }
        return false;
    }
}
