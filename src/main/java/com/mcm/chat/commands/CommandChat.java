package com.mcm.chat.commands;

import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.ChatStatus;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.enums.ServersList;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class CommandChat implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String[] perm = {"superior", "gestor"};

        if (command.getName().equalsIgnoreCase("chat")) {
            if (Arrays.asList(perm).contains(tag)) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("limpar")) {
                        for (Player target : Bukkit.getOnlinePlayers()) {
                            for (int i = 0; i < 100; i++) {
                                target.sendMessage(" ");
                            }
                            target.sendMessage(Main.getTradution("mn8!dpYXT8KJMRT", target.getUniqueId().toString()));
                        }
                        for (ServersList server : ServersList.values()) {
                            if (!com.mcm.core.Main.server_name.equals(server.name().replaceAll("_", "-")))
                                RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/chat_clear");
                        }
                    } else if (args[0].equalsIgnoreCase("on")) {
                        if (ChatStatus.getStatus() == false) {
                            ChatStatus.setStatus(true);
                            for (ServersList server : ServersList.values()) {
                                if (!com.mcm.core.Main.server_name.equals(server.name().replaceAll("_", "-")))
                                    RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/chat_status=true");
                            }
                            player.sendMessage(Main.getTradution("b79h@G3aMggFx!m", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("ncv&!aAXAv7k!mu", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else if (args[0].equalsIgnoreCase("off")) {
                        if (ChatStatus.getStatus() == true) {
                            ChatStatus.setStatus(false);
                            for (ServersList server : ServersList.values()) {
                                if (!com.mcm.core.Main.server_name.equals(server.name().replaceAll("_", "-")))
                                    RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/chat_status=false");
                            }
                            player.sendMessage(Main.getTradution("mNY2?j*3neM66JM", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(Main.getTradution("W$g84GmQ$5$D2GT", uuid));
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } else {
                        player.sendMessage(Main.getTradution("cZc%Wefv3YtwDRH", uuid));
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    }
                } else {
                    player.sendMessage(Main.getTradution("cZc%Wefv3YtwDRH", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                }
            }
        }
        return false;
    }
}
