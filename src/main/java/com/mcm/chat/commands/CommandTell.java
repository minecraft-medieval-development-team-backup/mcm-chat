package com.mcm.chat.commands;

import com.mcm.chat.cache.ChatTimer;
import com.mcm.chat.cache.TellSended;
import com.mcm.core.Main;
import com.mcm.core.RabbitMq;
import com.mcm.core.cache.TellReceived;
import com.mcm.core.database.RMqChatManager;
import com.mcm.core.database.TagDb;
import com.mcm.core.enums.ServersList;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Date;

public class CommandTell implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();
        String tag = TagDb.getTag(uuid);

        if (command.getName().equalsIgnoreCase("tell") || command.getName().equalsIgnoreCase("msg")) {
            if (ChatTimer.get(uuid + "tell") != null) {
                if ((new Date().getTime() - ChatTimer.get(uuid + "tell").getDate().getTime()) < 15) {
                    player.sendMessage(ChatColor.RED + Main.getTradution("eYX2S6Asfx*cYaY", uuid) + (new Date().getTime() - ChatTimer.get(uuid + "tell").getDate().getTime()) + Main.getTradution("tcyRRGrkh5uV8%x", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    return false;
                } else ChatTimer.get(uuid + "tell").delete();
            }
            if (TellReceived.get(uuid) == null) {
                if (args.length > 1) {
                    Player target = Bukkit.getPlayerExact(args[0]);

                    String[] perm = {"superior", "gestor", "moderador", "suporte"};
                    if (!Arrays.asList(perm).contains(tag)) new ChatTimer(uuid + "tell").insert();
                    if (target != null && target.isOnline()) {
                        String msg = String.join(" ", args).substring(args[0].length());
                        target.sendMessage(ChatColor.YELLOW + Main.getTradution("CPc6T47E@XYj7EQ", target.getUniqueId().toString()) + ChatColor.GRAY + player.getName() + Main.getTradution("anCjNd@E9Q$R@BV", target.getUniqueId().toString()) + msg);
                        target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.sendMessage(ChatColor.YELLOW + Main.getTradution("CPc6T47E@XYj7EQ", uuid) + ChatColor.GRAY + Main.getTradution("JA*GgfCb2W8!q7y", uuid) + target.getName() + ": " + msg);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    } else {
                        String msg = String.join(" ", args).substring(args[0].length());
                        player.sendMessage(ChatColor.YELLOW + Main.getTradution("CPc6T47E@XYj7EQ", uuid) + ChatColor.GRAY + Main.getTradution("JA*GgfCb2W8!q7y", uuid) + target.getName() + ": " + msg);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                        for (ServersList server : ServersList.values()) {
                            if (!com.mcm.core.Main.server_name.equals(server.name().replaceAll("_", "-")))
                                RabbitMq.send(server.name().replaceAll("_", "-"), RMqChatManager.key + "/tell=" + target.getName() + "=" + ChatColor.YELLOW + Main.getTradution("CPc6T47E@XYj7EQ", target.getUniqueId().toString()) + ChatColor.GRAY + player.getName() + Main.getTradution("anCjNd@E9Q$R@BV", target.getUniqueId().toString()) + msg);
                        }

                        new TellSended(uuid, player, args[0]).insert();
                    }
                }
            }
        }
        return false;
    }
}
