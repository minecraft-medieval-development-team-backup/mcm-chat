package com.mcm.chat.utils;

import org.bukkit.ChatColor;

public class ChatUtils {

    public static String permColor(String msg, String tag) {
        if (!tag.equals("membro")) {
            return ChatColor.translateAlternateColorCodes('&', msg);
        } else return msg.replaceAll("&", "");
    }

    public static String tagMsg(String tag) {
        if (!tag.equals("membro")) {
            return colorChatType(tag) + "[" + tagFormat(tag) + "] ";
        } else return null;
    }

    public static String tagFormat(String tag) {
        if (tag.equalsIgnoreCase("superior")) return "Superior";
        else if (tag.equalsIgnoreCase("gestor")) return "Gestor";
        else if (tag.equalsIgnoreCase("moderador")) return "Moderador";
        else if (tag.equalsIgnoreCase("suporte")) return "Suporte";
        else if (tag.equalsIgnoreCase("youtuber")) return "YouTuber";
        else if (tag.equalsIgnoreCase("vip")) return "VIP";
        return null;
    }

    public static ChatColor colorChatType(String tag) {
        if (tag.equalsIgnoreCase("superior")) return ChatColor.DARK_RED;
        else if (tag.equalsIgnoreCase("gestor")) return ChatColor.RED;
        else if (tag.equalsIgnoreCase("moderador")) return ChatColor.DARK_GREEN;
        else if (tag.equalsIgnoreCase("suporte")) return ChatColor.YELLOW;
        else if (tag.equalsIgnoreCase("youtuber")) return ChatColor.RED;
        else if (tag.equalsIgnoreCase("vip")) return ChatColor.GOLD;
        return null;
    }
}
