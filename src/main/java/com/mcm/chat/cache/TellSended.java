package com.mcm.chat.cache;

import com.mcm.core.Main;
import com.mcm.core.cache.TellReceived;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class TellSended {

    private String uuid;
    private Player player;
    private String name;
    private static HashMap<String, TellSended> cache = new HashMap<>();

    public TellSended (String uuid, Player player, String name) {
        this.uuid = uuid;
        this.player = player;
        this.name = name;

        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                if (TellReceived.get(TellSended.get(uuid).getName()) == null) {
                    TellSended.get(uuid).delete();
                    player.sendMessage(Main.getTradution("*UktGp4BvB#*6Bg", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                } else {
                    TellReceived.get(TellSended.get(uuid).getName()).delete();
                    TellSended.get(uuid).delete();
                }
            }
        }, 40L);
    }

    public TellSended insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static TellSended get(String uuid) {
        return cache.get(uuid);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
