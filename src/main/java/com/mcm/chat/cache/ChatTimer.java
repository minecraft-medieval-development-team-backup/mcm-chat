package com.mcm.chat.cache;

import java.util.Date;
import java.util.HashMap;

public class ChatTimer {

    private String uuid;
    private Date date;
    private static HashMap<String, ChatTimer> cache = new HashMap<>();

    public ChatTimer (String uuid) {
        this.uuid = uuid;
        this.date = new Date();
    }

    public ChatTimer insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static ChatTimer get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
