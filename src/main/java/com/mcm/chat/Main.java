package com.mcm.chat;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static String ip = com.mcm.core.Main.ip;
    public static String table = com.mcm.core.Main.table;
    public static String user = com.mcm.core.Main.user;
    public static String password = com.mcm.core.Main.password;

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        RegisterCommands.register();
        RegisterListeners.register();
        RegisterMain.register();
    }

    @Override
    public void onDisable() {
        //-
    }
}
