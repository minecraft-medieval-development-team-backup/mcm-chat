package com.mcm.chat.listeners;

import com.mcm.chat.utils.ChatUtils;
import com.mcm.core.Main;
import com.mcm.core.cache.ChatStatus;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.Arrays;

public class ChatLocalEvent implements Listener {

    @EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onSpeak(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        String tag = com.mcm.core.database.TagDb.getTag(uuid);

        String msg = event.getMessage();
        int distance = 100;
        boolean haveNext = false;

        event.setCancelled(true);

        if (com.mcm.core.database.ConfigDb.get(uuid).getChatLocal() == false) {
            player.sendMessage(Main.getTradution("%5jqEP!H62sQKcg", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            return;
        }

        String[] perm = {"superior", "gestor", "moderador", "suporte"};
        if (ChatStatus.getStatus() == false && !Arrays.asList(perm).contains(tag)) {
            player.sendMessage(Main.getTradution("T9ueabA*Edan@Ru", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            return;
        }

        for (Player target : Bukkit.getOnlinePlayers()) {
            if (!player.getName().equals(target.getName()) && player.getWorld().equals(target.getLocation().getWorld()) && target.getLocation().distance(player.getLocation()) <= distance && Bukkit.getOnlinePlayers().size() > 1) {
                if (com.mcm.core.database.ConfigDb.get(target.getUniqueId().toString()).getChatLocal() && !target.getName().equals(player.getName())) {
                    if (ChatUtils.tagMsg(tag) != null) {
                        target.sendMessage(ChatColor.YELLOW + "[L] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
                    } else {
                        target.sendMessage(ChatColor.YELLOW + "[L] " + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
                    }
                    haveNext = true;
                }
            }
        }

        if (haveNext == false) {
            player.sendMessage(Main.getTradution("@eNsqs7A9SqB?6v", uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        } else {
            if (ChatUtils.tagMsg(tag) != null) {
                player.sendMessage(ChatColor.YELLOW + "[L] " + ChatUtils.tagMsg(tag) + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
            } else {
                player.sendMessage(ChatColor.YELLOW + "[L] " + ChatColor.GRAY + player.getName() + ": " + ChatColor.YELLOW + ChatUtils.permColor(msg, tag));
            }
        }
    }
}
