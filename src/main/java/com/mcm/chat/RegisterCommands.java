package com.mcm.chat;

import com.mcm.chat.commands.CommandChat;
import com.mcm.chat.commands.CommandGlobal;
import com.mcm.chat.commands.CommandStaff;
import com.mcm.chat.commands.CommandTell;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("global").setExecutor(new CommandGlobal());
        Main.instance.getCommand("g").setExecutor(new CommandGlobal());
        Main.instance.getCommand("staff").setExecutor(new CommandStaff());
        Main.instance.getCommand("s").setExecutor(new CommandStaff());
        Main.instance.getCommand("chat").setExecutor(new CommandChat());
        Main.instance.getCommand("tell").setExecutor(new CommandTell());
        Main.instance.getCommand("msg").setExecutor(new CommandTell());
    }
}
